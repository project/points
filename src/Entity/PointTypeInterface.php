<?php

namespace Drupal\points\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface for defining Point type entities.
 */
interface PointTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {}
