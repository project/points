<?php

namespace Drupal\points\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Point entity.
 *
 * @ingroup points
 *
 * @ContentEntityType(
 *   id = "point",
 *   label = @Translation("Point"),
 *   bundle_label = @Translation("Point type"),
 *   handlers = {
 *     "access" = "Drupal\points\Access\PointAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\points\PointListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\points\Form\PointForm",
 *       "add" = "Drupal\points\Form\PointForm",
 *       "edit" = "Drupal\points\Form\PointForm",
 *       "delete" = "Drupal\points\Form\PointDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "inline_form" = "Drupal\points\Form\PointInlineForm",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer point entities",
 *   base_table = "point",
 *   data_table = "point_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/points/{point}",
 *     "collection" = "/admin/structure/points/overview",
 *     "add-page" = "/admin/structure/points/add",
 *     "add-form" = "/admin/structure/points/add/{point_type}",
 *     "edit-form" = "/admin/structure/points/{point}/edit",
 *     "delete-form" = "/admin/structure/points/{point}/delete",
 *     "delete-multiple-form" = "/admin/structure/points/delete",
 *   },
 *   bundle_entity_type = "point_type",
 *   field_ui_base_route = "entity.point_type.edit_form",
 *   constraints = {
 *     "PointState" = {}
 *   }
 * )
 */
class Point extends ContentEntityBase implements PointInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getPoints() {
    return $this->get('points')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPoints($points) {
    $this->set('points', $points);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLog() {
    return $this->get('log')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLog($log) {
    $this->set('log', $log);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['log'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Log'))
      ->setDescription(t('The description of the pending movement.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setCustomStorage(TRUE);

    $fields['state'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('State'))
      ->setDescription(t('The state of Point; when the state is the same as the current points field, it is valid'))
      ->setSettings([
        'default_value' => '0',
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setCustomStorage(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::postSave($storage);

    if ($this->isNew()) {
      $original_point = 0;
      $this->isNew = TRUE;
    }
    else {
      $original_point = $this->original->get('points')->value;
    }

    $new_point = $this->get('points')->value;
    if ($original_point != $new_point) {
      $this->point_delta = $new_point - $original_point;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if (isset($this->point_delta)) {
      $query = \Drupal::entityTypeManager()->getStorage('point')->getQuery();
      $query->accessCheck(FALSE);
      $query->condition('id', $this->id());

      if ($result = $query->execute()) {
        $points = $this->point_delta;
        $this->createTransaction($this->id(), $points, 0, $this->getLog());
      }
    }
  }

  /**
   * Helper to create a new point movement entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   */
  public function createTransaction($point_id, $points, $uid = 0, $des = '') {
    $movement = $this->entityTypeManager()
      ->getStorage('point_movement')
      ->create([
        'point_id' => $point_id,
        'points' => $points,
        'uid' => empty($uid) ? \Drupal::currentUser()->id() : 0,
        'description' => $des,
      ]);

    $movement->save();
  }

}
