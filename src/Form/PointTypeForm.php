<?php

namespace Drupal\points\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for point type add/edit forms.
 *
 * @package Drupal\points\Form
 */
class PointTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\points\Entity\PointTypeInterface $point_type */
    $point_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $point_type->label(),
      '#description' => $this->t('Label for the point type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $point_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\points\Entity\PointType::load',
      ],
      '#disabled' => !$point_type->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $point_type->getDescription(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save point type');

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $point_type = $this->entity;
    $status = $point_type->save();

    switch ($status) {
      case SAVED_NEW:
        points_add_number_field($point_type);
        $this->messenger()->addStatus($this->t('Created the %label Point type.', [
          '%label' => $point_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Point type.', [
          '%label' => $point_type->label(),
        ]));

    }

    $form_state->setRedirectUrl($point_type->toUrl('collection'));
    return $status;
  }

}
