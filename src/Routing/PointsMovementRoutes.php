<?php

namespace Drupal\points\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes for the module.
 */
class PointsMovementRoutes implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PointsMovementRoutes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Provides dynamic routes.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function routes() {
    $routes = [];
    $config_entities = $this->entityTypeManager->getStorage('field_storage_config')->loadMultiple();
    foreach ($config_entities as $config_entity) {
      if ($config_entity->get('type') === 'entity_reference' && $config_entity->get('settings')['target_type'] === 'point') {
        $entity_type_id = $config_entity->get('entity_type');
        $field_name = substr($config_entity->get('field_name'), 6);

        $routes['entity.' . $entity_type_id . '.points.movement.' . $field_name] = new Route(
          "/{$entity_type_id}/{{$entity_type_id}}/{$field_name}",
          [
            '_controller' => '\Drupal\points\Controller\EntityPointsMovementController::page',
            '_title' => $field_name,
          ],
          [
            '_permission'  => 'view point entities',
          ]
        );
      }
    }

    return $routes;
  }

}
