<?php

namespace Drupal\points\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the PointState constraint.
 */
class PointStateConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $entityTypeManager;

  /**
   * Constructs a PointStateConstraintValidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (isset($entity)) {
      /** @var \Drupal\points\Entity\PointInterface $entity */
      if (!$entity->isNew()) {
        $state = $entity->getState();
        $saved_point = $this->entityTypeManager->getStorage('point')->loadUnchanged($entity->id());

        if ($saved_point && $state != $saved_point->getPoints()) {
          $this->context->addViolation($constraint->message);
        }
      }
    }
  }

}
