<?php

namespace Drupal\points\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for all entity bundles.
 */
class PointsLocalTask extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * Returns the router.route_provider service.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routProvider;

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $entityTypeManager;

  /**
   * Creates an PointsLocalTask object.
   *
   * @param \Drupal\Core\Routing\RouteProvider $route_provider
   *   A Route Provider front-end for all Drupal-stored routes.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(RouteProvider $route_provider, EntityTypeManagerInterface $entity_type_manager) {
    $this->routProvider = $route_provider;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('router.route_provider'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $derivatives = [];
    $config_entities = $this->entityTypeManager->getStorage('field_config')->loadMultiple();

    foreach ($config_entities as $config_entity) {
      if ($settings = $config_entity->getSetting('handler')) {
        $handler = explode(':', $settings);

        if (isset($handler[1]) && $handler[1] === 'point' && $config_entity->getType() === 'entity_reference') {
          $entity_type_id = $config_entity->get('entity_type');
          $field_name = substr($config_entity->get('field_name'), 6);

          $derivatives[$entity_type_id . '.points.' . $field_name] = [
            'route_name' => "entity.$entity_type_id.points.movement.$field_name",
            'title' => $config_entity->getLabel(),
            'base_route' => 'entity.' . $entity_type_id . '.canonical',
            'weight' => 50,
          ];
        }
      }
    }

    foreach ($derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $derivatives;
  }

}
