<?php

namespace Drupal\points\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Class DeletePointFieldCheck. Access handler for config field.
 */
class PointFieldDeleteCheck implements AccessInterface {

  /**
   * Make the points field non-deleteable.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $field = $route_match->getParameter('field_config');
    $field_name = $field->get('field_name');

    return 'points' == $field_name ? AccessResult::forbidden() : AccessResult::allowed();
  }

}
