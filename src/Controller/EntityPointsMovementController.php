<?php

namespace Drupal\points\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\Renderer;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base controller for dynamic routes.
 */
class EntityPointsMovementController extends ControllerBase {

  /**
   * Returns the renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Returns the path.current service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a EntityPointsMovementController object.
   *
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Turns a render array into a HTML string.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   Represents the current path for the current request.
   */
  public function __construct(Renderer $renderer, CurrentPathStack $current_path) {
    $this->renderer = $renderer;
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('path.current')
    );
  }

  /**
   * Provides callback for dynamic route.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   * @throws \Exception
   */
  public function page() {
    // The current_path should look like /entity_type/entity_id/points.
    $path = explode('/', $this->currentPath->getPath());

    $entity = $this->entityTypeManager()->getStorage($path[1])->load($path[2]);
    $config_entities = $this->entityTypeManager()->getStorage('field_storage_config')->loadMultiple();

    $target_id = NULL;
    foreach ($config_entities as $config_entity) {
      $field_name = $config_entity->get('field_name');
      if ($config_entity->get('type') === 'entity_reference' && $config_entity->get('settings')['target_type'] === 'point' && $config_entity->get('entity_type') == $path[1] && substr($field_name, 6) == $path[3]) {
        $target_id = $entity->{$field_name}->target_id;
      }
    }

    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('point_movement');
    $view_render_array = $view->buildRenderable('embed', [$target_id]);

    return [
      '#type' => 'markup',
      '#markup' => $this->renderer->render($view_render_array),
    ];
  }

}
